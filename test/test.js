const request = require('supertest');
var chai = require('chai');
var expect = chai.expect;
var assert = chai.assert;
const app = require('../app');

var maths = require('../utils/maths.js');

describe('App', function() {

  it('should sum up 2 numbers manually', function() {
    expect(1 + 1).to.equal(2);
    expect(2 + 1).to.equal(3);
    expect(4 - 2).to.equal(2);
  });

  it('should sum up 2 numbers correctly with maths library', function() {
    expect(maths.sum(1, 1)).to.equal(2);
    expect(maths.sum(-1, 3)).to.equal(2);
    expect(maths.sum(4, -2)).to.equal(2);
    expect(maths.sum(0, 2)).to.equal(2);
    expect(maths.sum(2, 0)).to.equal(2);
    expect(maths.sum(-2, -3)).to.equal(-5);
  });

});
